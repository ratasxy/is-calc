import java.util.*;

class Calc
{
	public static void main(String[] args)
	{
		int a;
		int b;
		char op;

		Scanner scanner = new Scanner(System.in);
		Calc calc = new Calc();
		while(true){
			a = scanner.nextInt();
			op = scanner.next(".").charAt(0);
			b = scanner.nextInt();
			
			if(op == 'e')
				System.exit(0);

			System.out.println(calc.operate(a, b, op));				}	
	}

	public int operate(int a, int b, char op)
	{
		int ans = 0;
		switch (op){
			case '+':
				ans = a+b;
				break;
			case '-':
				ans = a-b;
				break;
			case '*':
				ans = a*b;
				break;
			case '/':
				ans = a/b;
				break;
			case '%':
				ans = a%b;
				break;
		}

		return ans;
	}
}
